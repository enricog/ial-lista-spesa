import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { AppComponent } from './app.component';
import { AddBarComponent } from './COMPONENTS/ADD-BAR/add-bar.component';
import { ListItemComponent } from './COMPONENTS/list-item/list-item.component';
import { ListComponent } from './COMPONENTS/list/list.component';
import { CapitalizePipe } from './pipes/capitalize.pipe';
import { TakenPipe } from './pipes/taken.pipe';
import { ItemService } from './services/item.service';
import { UntakenPipe } from './pipes/untaken.pipe';

@NgModule({
  declarations: [
    AppComponent,
    AddBarComponent,
    ListItemComponent,
    ListComponent,
    CapitalizePipe,
    TakenPipe,
    UntakenPipe,
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [
    ItemService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
