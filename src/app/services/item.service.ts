import { Injectable } from '@angular/core';
import { ItemModel } from '../models/item.model';

@Injectable()
export class ItemService{
    public lista: ItemModel[] = [
        {
            testo: 'banana',
            preso: false
        },
        {
            testo: 'uova',
            preso: true
        }
    ];

    public add(text: string){
        // this.lista.push({
        //     testo: text,
        //     preso: true
        //   });

        this.lista = [...this.lista, {
            testo: text,
            preso: true
        }];
    }

    public remove(item: ItemModel){
        // let index = this.lista.findIndex(x => x === item);
        // if (index >= 0) {
        //     this.lista.splice(index,1);
        // }

        this.lista = this.lista.filter(x => x !== item);
    }
}