import { Pipe, PipeTransform } from '@angular/core';
import { ItemModel } from '../models/item.model';

@Pipe({
  name: 'taken'
})
export class TakenPipe implements PipeTransform {

  transform(value: ItemModel[]): ItemModel[] {

    if(!value)
    return value;

    return value.filter(x => x.preso);

    // let ret: ItemModel[] = [];

    // for(let x of value){
    //   if(x.preso){
    //     ret.push(x);
    //   }
    // }

    // return ret;
  }

}
