import { Pipe, PipeTransform } from '@angular/core';
import { ItemModel } from '../models/item.model';

@Pipe({
  name: 'untaken'
})
export class UntakenPipe implements PipeTransform {

  transform(value: ItemModel[]): ItemModel[] {
    if(!value)
      return value;

    return value.filter(x => !x.preso);
  }

}
