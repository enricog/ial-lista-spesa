import { Component } from '@angular/core';
import { ItemModel } from './models/item.model';
import { ItemService } from './services/item.service';

@Component({
  selector: 'pippo-pluto',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    constructor(public servizio: ItemService){

    }
}
