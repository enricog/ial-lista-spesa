import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ItemModel } from 'src/app/models/item.model';

@Component({
  selector: 'app-list-item',
  template: `
    <input type="checkbox" [(ngModel)]="item.preso"/>
    <span [ngClass]="{'done': item.preso, 'miss': !item.preso}">
      {{item.testo | capitalize}}
    </span> 
    <a (click)="removeMe.emit(item)"> X</a>
  `,
  styles: [
    '.done {color: palevioletred; text-decoration: line-through;}',
    '.miss {color: green;}'
  ]
})
export class ListItemComponent implements OnInit {
  @Input() item: ItemModel;
  @Output() removeMe = new EventEmitter<ItemModel>();

  constructor() { }

  ngOnInit() {
  }

}