import { Component, Output, EventEmitter } from "@angular/core";



@Component({
    selector: 'add-bar',
    template: `
        <input [(ngModel)]="newItem" type="text">
        <button (click)="add()">Aggiungi</button>
    `
})
export class AddBarComponent{
    @Output() addItem = new EventEmitter<string>();

    newItem: string;

    add(){
        this.addItem.emit(this.newItem);
    }
}