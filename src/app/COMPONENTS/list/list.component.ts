import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ItemModel } from 'src/app/models/item.model';

@Component({
  selector: 'app-list',
  template: `
    <ul>
      <li *ngFor="let i of items">
        <app-list-item [item]="i" (removeMe)="removeItem.emit($event)"></app-list-item>
      </li>
    </ul>
  `,
  styles: []
})
export class ListComponent implements OnInit {
  @Input() items: ItemModel[];
  @Output() removeItem = new EventEmitter<ItemModel>();

  constructor() { }

  ngOnInit() {
  }

}
